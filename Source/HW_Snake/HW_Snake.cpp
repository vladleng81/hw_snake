// Copyright Epic Games, Inc. All Rights Reserved.

#include "HW_Snake.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, HW_Snake, "HW_Snake" );
