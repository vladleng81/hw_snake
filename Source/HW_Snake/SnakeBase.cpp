// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.0f;
	MovementSpeed = 0.7f;
	LastMoveDirection = EMovementDirection::UP;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		auto NewLocation = FVector(SnakeElements.Num() * ElementSize, 0, 0);
		auto NewTransform = FTransform(GetActorLocation() - NewLocation);
		ASnakeElementBase* SnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		int32 ElemIndex = SnakeElements.Add(SnakeElem);

		if (ElemIndex == 0)
		{
			SnakeElem->SetFirstElementType();
		}
	}
	
}

void ASnakeBase::Move()
{
	FVector MovementVector;
	auto Offset = ElementSize;

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP :
		MovementVector.X += Offset;
		break;
	case EMovementDirection::DOWN :
		MovementVector.X -= Offset;
		break;
	case EMovementDirection::LEFT :
		MovementVector.Y += Offset;
		break;
	case EMovementDirection::RIGHT :
		MovementVector.Y -= Offset;
		break;
	}

	//AddActorWorldOffset(MovementVector);
	for (int i = SnakeElements.Num() - 1; i > 0; --i)
	{
		auto CurElem = SnakeElements[i];
		auto PrevElem = SnakeElements[i - 1];
		FVector PrevLocation = PrevElem->GetActorLocation();
		CurElem->SetActorLocation(PrevLocation);
	}

	// Head is moving along from other elements
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
}

